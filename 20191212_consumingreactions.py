#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 09:15:01 2019

Question: how much does the CS changes if the ATP and NADPH consuming reactions are changed

@author: anna
"""

from model import *
import matplotlib.pyplot as plt

s.simulate(1000)

plt.figure()
plt.title('Light: '+ str(m.get_parameter('PFD')))
plt.plot(s.get_time(), s.get_variable("PQ"))
s.plot_flux_selection(["vPS2", "vCyc", "vSt12"])

plt.figure()
s.plot_flux_selection(["vPS2", "vCyc", "vSt12"])
s.plot_selection(["Q"])

s = Simulator(m)

# Set initial conditions using dictionary
y0 = {"B":  m.get_parameter("PSIItot"),  #photosystem II protein concentration
    "PQ": 8.5,  # oxidised plastoquinone
    "PC": 2.,  # oxidised plastocyan
    "Fd": 2.5,  # oxidised ferrodoxin
    "E": 0.1,
    "ATP": 30.,  # stromal concentration of ATP
    "NADPH": 7.,  # stromal concentration of NADPH
    "H": 0.0002,  # lumenal protons
    "LHC": 0.9,  # non-phosphorylated antenna
    "Psbs": 0.9, # PsBs
    "Vx": 0.9}

s.set_initial_conditions(y0)
    
m.update_parameters({"PFD":500})
s.simulate(1000)

# variables in double format: oxiodised and reduced
plt.figure()
plt.title('Light: '+ str(m.get_parameter('PFD')))
plt.plot(s.get_time(), s.get_variable("PQ"))

s.plot_flux_selection(["vPS2", "vCyc", "vSt12"])

plt.figure()

s.plot_flux_selection(["vPS2", "vCyc", "vSt12"])
s.plot_selection(["Q"])