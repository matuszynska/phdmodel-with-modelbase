
__author__ = 'anna'
# draw the data from Ryutari and compare to Arabidopsis

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('./ChlamyAbs/cell', delimiter='')
wavenm = data[:,0]  # wavelength [nm]
abs = data[:,1]

plt.plot(wavenm, abs/max(abs))

data = np.genfromtxt('./ChlamyAbs/LHCII', delimiter='')
wavenmlhc2 = data[:,0]  # wavelength [nm]
abslhc2 = data[:,1]

data = np.genfromtxt('./ChlamyAbs/LHCI-PSI', delimiter='')
wavenmps1sc = data[:,0]  # wavelength [nm]
absps1sc = data[:,1]

data = np.genfromtxt('./ChlamyAbs/PSII-LHCII', delimiter='')
wavenmps2sc = data[:,0]  # wavelength [nm]
absps2sc = data[:,1]

plt.figure()
plt.plot(wavenmlhc2, abslhc2/max(abslhc2), linewidth=2, color='r', label='LHCII')
plt.plot(wavenmps1sc, absps1sc/max(absps1sc), linewidth=2, color='orange', label='PSI-LHCI')
plt.plot(wavenmps2sc, absps2sc/max(absps2sc), linewidth=2, color='k', label='PSII-LHCII')
plt.legend(ncol=3, loc='center', title="Complexes", bbox_to_anchor=[0.5, 1],
                   columnspacing=1.0, labelspacing=0.0, handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True, fontsize=20)
plt.xlabel('wavelengths [nm]')
plt.ylabel('absorption [a.u.]')
plt.xlim(345, 800)




import csv
import numpy as np
import matplotlib.pyplot as plt

class ComplexAbs():
    """
    The class provides routines to calculate relative absorption coefficients for:
    PSII-core, PSI-LHCI supercomplex and LHCII trimers separately

    The absorptia for seperate complexes are obtained from the lab of R. Bassi (Verona)
    The absorption of WT Chlamydomonas is obtained from the lab of J. Minagawa (Okazaki)

    """
    def __init__(self):
        data = np.genfromtxt('./ChlamyAbs/cell', delimiter='')
        self.wavelengths = data[:,0]  # wavelength [nm]
        self.abs = data[:,1]

        data = np.genfromtxt('./ChlamyAbs/LHCII', delimiter='')
        abslhc2 = data[:,1]

        data = np.genfromtxt('./ChlamyAbs/LHCI-PSI', delimiter='')
        absps1sc = data[:,1]

        data = np.genfromtxt('./ChlamyAbs/PSII-LHCII', delimiter='')
        absps2sc = data[:,1]

        self.LHCIIspectra = abslhc2
        self.PSIIspectra = absps2sc#[np.average([self.data[i, 3], self.data[i, 4]]) for i in range(len(self.wavelengths))]
        self.PSIspectra = absps1sc

        # # Based on the numbers provided by Unlu...Croce (PNAS, 2014)
        # self.PSI_a = 196.0
        # self.PSII_a = 52.0
        # self.LHCII_a = 24.0
        # self.trimers = 7.0
        #
        # self.tot_a = self.PSI_a + self.PSII_a + self.trimers * self.LHCII_a
        #
        # # Chlorophyll content depending factor
        # self.chla_PSII_f = self.PSII_a / self.tot_a
        # self.chla_PSI_f = self.PSI_a / self.tot_a
        # self.chla_LHCII_f = self.trimers * self.LHCII_a / self.tot_a
    #
    # def findlocalmax(self, spectra, minr, maxr):
    #     """ finds the local maximum within the range described by minf and maxr"""
    #     indexes = np.where((self.wavelengths > minr) & (self.wavelengths < maxr))
    #     if spectra.startswith('LHCII'):
    #         sp = self.LHCIIspectra
    #     elif spectra.startswith('PSII'):
    #         sp = self.PSIIspectra
    #     elif spectra.startswith('PSI'):
    #         sp = self.PSIspectra
    #     peak = max(sp[min(indexes[0]):max(indexes[0])])
    #     ind = np.where(sp == peak)
    #     wave = self.wavelengths[ind]
    #     return peak, ind, wave
    #
    # def get_a_peaks(self):
    #     """
    #     :return: array with peaks corresponding to chlorophyll a around 680 nm for PSII, PSI and LHCII
    #     used for normalization to chlorophylls a absorption
    #     """
    #     a_peak_PSII = self.findlocalmax('PSIIspectra', 670, 690)[0]
    #     a_peak_PSI = self.findlocalmax('PSIspectra', 670, 690)[0]
    #     a_peak_LHCII = self.findlocalmax('LHCIIspectra', 670, 690)[0]
    #     return [a_peak_PSII, a_peak_PSI, a_peak_LHCII]
    #
    # def get_normalized_spectra(self):
    #     """
    #     :return: three spectra normalized to the peak corresponding to the absorption of chls a
    #     """
    #     [a_peak_PSII, a_peak_PSI, a_peak_LHCII] = self.get_a_peaks()
    #     normalizedPSII = self.PSIIspectra / a_peak_PSII
    #     normalizedPSI = self.PSIspectra / a_peak_PSI
    #     normalizedLHCII = self.LHCIIspectra / a_peak_LHCII
    #
    #     return [normalizedPSII, normalizedPSI, normalizedLHCII]

    def get_specific_abs(self, nm):
        """
        :param nm: wavelength for which we want to calculate the coefficients
        :return: absorption coefficient for PSII-core, PSI-LHCI supercomplex and mobile antennae (LHCII)
        """
        index = np.where(self.wavelengths == nm)[0]
        #/max(abs[index])
        coeff_PSII = self.abs[index] * self.PSIIspectra[index]/max(self.PSIIspectra)
        coeff_PSI = self.abs[index] * self.PSIspectra[index]/max(self.PSIspectra)
        coeff_LHCII = self.abs[index] * self.LHCIIspectra[index]/max(self.LHCIIspectra)

        return [coeff_PSII, coeff_PSI, coeff_LHCII]

    def load_strain_abs(self, strain):
        file='/home/anna/ExperimentalData/201505_OkazakiExp/strain_abs/' + strain + '.txt'
        data = np.genfromtxt(file, delimiter='\t')
        waves = data[:,0]
        abs = data[:,1]
        return data

    def get_lambda_coef(self, wavelength):
        data = self.load_strain_abs()
        lambda_coef = data[np.where(wavelength == data[:, 0]), 1]
        return lambda_coef