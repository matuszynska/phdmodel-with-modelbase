#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 10:34:18 2019

@author: anna
"""
run model
import matplotlib.pyplot as plt


m.update_parameters({"PFD":100})
plt.title('Light: '+ str(m.get_parameter('PFD')))

for i in [360, 400, 445, 460, 488, 510, 550,630,650,670,690,720]:
    s = Simulator(m)
    m.update_parameters({"wv":i, "kcyc":1.})#, "kStt7":0., "kPph1":0.})
    s.set_initial_conditions(y0)
    Y = s.simulate_to_steady_state()
    a = dict(zip(y0, Y))
    
    plt.plot(i, m.get_full_concentration_dict(a)['ps1cs']/m.get_full_concentration_dict(a)['ps2cs'], 'rd')
    
plt.show()

