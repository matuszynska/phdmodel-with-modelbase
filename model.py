#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 26 22:58:12 2019

@author: anna
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import spectra

c = spectra.ComplexAbs()

from modelbase.ode import Model, Simulator

compounds = [
        "B",  #photosystem II protein concentration
        "PQ",  # oxidised plastoquinone
        "PC",  # oxidised plastocyan
        "Fd",  # oxidised ferrodoxin
        "E",    #activation of the ATPase
        "ATP",  # stromal concentration of ATP
        "NADPH",  # stromal concentration of NADPH
        "H",  # lumenal protons
        "LHC",#,  # non-phosphorylated antenna
        "Psbs", # PsBs
        "Vx"]
#, # violaxanthin (as ratio of all xantophylls)
#        "Light"]

p = {"PSIItot": 2.5, # [mmol/molChl] total concentration of PSII
    "PSItot": 2.5,
    "PQtot": 17.5, # [mmol/molChl]
    "PCtot": 4., # Bohme1987 but other sources give different values - seems to depend greatly on organism and conditions
    "Fdtot": 5., # Bohme1987
    "Ctot": 2.5, #source unclear (Schoettler says 0.4...?, but plausible to assume that complexes (PSII,PSI,b6f) have approx. same abundance)
    "NADPtot": 25., # estimate from ~ 0.8 mM, Heineke1991
    "APtot": 60., # [mmol/molChl] Bionumbers ~2.55mM (=81mmol/molChl) (FIXME: Soma had 50)
    "PsbStot": 1., # relative pool of PsbS
    "Xtot": 1., # relative pool of carotenoids (V+A+Z)
    "ATPasetot": 1., # relative pool of ATPase

    # parameters associated with photosystem II
    "kH": 5e9,
    "kH0": 5e8, # base quenching" after calculation with Giovanni
    "kF": 6.25e8, # 6.25e7 fluorescence 16ns
    "k1": 5e9, # excitation of Pheo / charge separation 200ps
    "k1rev": 1e10,
    "k2": 5e10, # original 5e9 (charge separation limiting step ~ 200ps) - made this faster for higher Fs fluorescence
    "kdeg": 100,    # rate of PSII damage corresponds to p.k2 / .5e8
    "krep": 5.55e-4, # rate of repair fo PSII

    # parameters associated with photosystem I
    "kStt7": 0.0035, # [s-1] fitted to the FM dynamics
    "kPph1": 0.0013, # [s-1] fitted to the FM dynamics
    "KM_ST": 0.2, # Switch point (half-activity of Stt7) for 20% PQ oxidised (80% reduced)
    "n_ST": 2., # Hill coefficient of 4 -> 1/(2.5^4)~1/40 activity at PQox=PQred
    "staticAntI": 0.37,     # corresponds to PSI - LHCI supercomplex, when chlorophyll decreases more relative fixed antennae
    "staticAntII": 0.1,     # corresponds to PSII core
    "prob_attach": 1.,            # probability of antena attaching to PSI


     # ATP and NADPH parameters
    "kActATPase": 0.05,  # on 14.09 increased from 0.01 to saturate between 1-2 min, not 10
                            # paramter relating the rate constant of activation of the ATPase in the light
    "kDeactATPase": 0.002,   # paramter relating the deactivation of the ATPase at night
    "kATPsynth": 20.,    # taken from MATLAB
    "kATPcons": 10.,     # taken from MATLAB
    "ATPcyt": 0.5,       # only relative levels are relevant (normalised to 1) to set equilibrium
    "Pi_mol": 0.01,
    "DeltaG0_ATP": 30.6, # 30.6kJ/mol / RT
    "HPR": 14./3.,  #Vollmar et al. 2009 (after Zhu et al. 2013)
    "kNADPHcons": 15., # taken from MATLAB
    "NADPHcyt": 0.5, # only relatice levels

    # global conversion factor of PFD to excitation rate
    #"cPFD": 4. # [m^2/mmol PSII]

    # pH and protons
    "pHstroma": 7.8,
    "kLeak": 10.,#0.010, # [1/s] leakage rate -- inconsistency with Kathrine
    "bH": 100., # proton buffer: ratio total / free protons

    # rate constants
    "kPQred": 250., # [1/(s*(mmol/molChl))]
    "kCytb6f": 2.5, # a rough estimate: transfer PQ->cytf should be ~10ms
    "kPTOX": .01, # ~ 5 electrons / seconds. This gives a bit more (~20)
    "kPCox": 2500., # a rough estimate: half life of PC->P700 should be ~0.2ms
    "kFdred": 2.5e5, # a rough estimate: half life of PC->P700 should be ~2micro-s
    "kcatFNR": 500., # Carrillo2003 (kcat~500 1/s)
    "kcyc": 1.,

    "O2ext": 8., # corresponds to 250 microM cor to 20%
    "kNDH": .002, # re-introduce e- into PQ pool. Only positive for anaerobic (reducing) condition
    "kNh": 0.05,
    "kNr": 0.004,
    "NPQsw": 5.8,
    "nH": 5.,

    "EFNR": 3., # Bohme1987
    "KM_FNR_F": 1.56, # corresponds to 0.05 mM (Aliverti1990)
    "KM_FNR_N": 0.22, # corresponds to 0.007 mM (Shin1971 Aliverti2004)

    # quencher fitted parameters
    "gamma0": 0.1,          # slow quenching of Vx present despite lack of protonation
    "gamma1": 0.25,         # fast quenching present due to the protonation
    "gamma2": 0.6,          # slow quenching of Zx present despite lack of protonation
    "gamma3": 0.15,         # fastest possible quenching

    # non-photochemical quenching PROTONATION
    "kDeprotonation": 0.0096,
    "kProtonationL": 0.0096,
    "kphSatLHC": 5.8,
    "nH": 5.,
    "NPQsw": 5.8,

    # non-photochemical quenching XANTOPHYLLS
    "kDeepoxV": 0.0024,
    "kEpoxZ": 0.00024,      # 6.e-4        # converted to [1/s]
    "kphSat": 5.8,          # [-] half-saturation pH value for activity de-epoxidase highest activity at ~pH 5.8
    "kHillX": 5.,     # [-] hill-coefficient for activity of de-epoxidase
    "kHillL": 3.,     # [-] hill-coefficient for activity of de-epoxidase
    "kZSat": 0.12,          # [-] half-saturation constant (relative conc. of Z) for quenching of Z

    # standard redox potentials (at pH=0) in V
    "E0_QA": -0.140,
    "E0_PQ": 0.354,
    "E0_cytf": 0.350,
    "E0_PC": 0.380,
    "E0_P700": 0.480,
    "E0_FA": -0.550,
    "E0_Fd": -0.430,
    "E0_NADP": -0.113,

    # physical constants
    "F": 96.485, # Faraday constant
    "R": 8.3e-3, # universal gas constant
    "T": 298., # Temperature in K - for now assumed to be constant at 25 C

    # light
    "PFD": 100.,
    "Ton": 360,
    "Toff": 1800,
    "dT": 120,

    "ox": True,
    "wv": False
        }

 
# ====================================================================== #
# Composed parameters #
# ====================================================================== #
def RT(p):
    return p.R*p.T

def dG_pH(p):
    return np.log(10)*RT(p)

def Hstroma(p):
    return 3.2e4*10**(-p.pHstroma) 

def kProtonation(p):
    return 4e-3 / Hstroma(p)

def pH(x):
    return (-np.log(x*(2.5e-4))/np.log(10))

def pHstroma(x):
    return (-np.log(x*(3.2e-5))/np.log(10))    

def pHinv(x):
    return (4e3*10**(-x))

def Keq_PQred(p):
    DG1 = -p.E0_QA * p.F
    DG2 = -2 * p.E0_PQ * p.F
    DG = -2 * DG1 + DG2 + 2 * p.pHstroma * dG_pH(p)
    K = np.exp(-DG/RT(p))
    return K

def Keq_cyc(p):
    DG1 = -p.E0_Fd * p.F
    DG2 = -2 * p.E0_PQ * p.F
    DG = -2 * DG1 + DG2 + 2 * dG_pH(p) * p.pHstroma
    K = np.exp(-DG/RT(p))
    return K

def Keq_cytfPC(p):
    DG1 = -p.E0_cytf * p.F
    DG2 = -p.E0_PC * p.F
    DG = -DG1 + DG2
    K = np.exp(-DG/RT(p))
    return K

def Keq_FAFd(p):
    DG1 = -p.E0_FA * p.F
    DG2 = -p.E0_Fd * p.F
    DG = -DG1 + DG2
    K = np.exp(-DG/RT(p))
    return K

def Keq_PCP700(p):
    DG1 = -p.E0_PC * p.F
    DG2 = -p.E0_P700 * p.F
    DG = -DG1 + DG2
    K = np.exp(-DG/RT(p))
    return K

def Keq_NDH(p):
    DG1 = -2 * p.E0_NADP * p.F
    DG2 = -2 * p.E0_PQ * p.F
    DG = -DG1 + DG2 + dG_pH(p) * p.pHstroma
    K = np.exp(-DG/RT(p))
    return K

def Keq_FNR(p):
    DG1 = -p.E0_Fd * p.F
    DG2 = -2 * p.E0_NADP * p.F
    DG = -2 * DG1 + DG2 + dG_pH(p) * p.pHstroma
    K = np.exp(-DG/RT(p))
    return K

def Keq_ATP(p, pH):
    DG = p.DeltaG0_ATP - dG_pH(p) * p.HPR * (p.pHstroma - pH)
    Keq = p.Pi_mol * np.exp(-DG/RT(p))
    #Keq = np.exp(-DG/RT(p))
    return Keq

def Keq_cytb6f(p, pH):
    DG1 = -2 * p.F * p.E0_PQ
    DG2 = -p.F * p.E0_PC
    DG = - (DG1 + 2*dG_pH(p) * pH) + 2 * DG2 + 2*dG_pH(p) * (p.pHstroma - pH)
    Keq = np.exp(-DG/RT(p))
    return Keq

# ====================================================================== #
# Conserved quantities -> for algebraic module #
# ====================================================================== #
def pqmoiety(p, PQ):
    return [p.PQtot - PQ]

def pcmoiety(p, PC):
    return [p.PCtot - PC]

def fdmoiety(p, Fd):
    return [p.Fdtot - Fd]

def adpmoiety(p, ATP):
    return [p.APtot - ATP]

def nadpmoiety(p, NADPH):
    return [p.NADPtot - NADPH]

def atpenzmoiety(p, E):
    return [p.ATPasetot - E]

def crosssections(p, LHC):
    """ calculates the cross section of PSII """
    free = (1 - p.prob_attach) * (1 - p.staticAntII - p.staticAntI) * (1-LHC)
    cs1 = p.staticAntI + p.prob_attach * (1 - p.staticAntII - p.staticAntI) * (1-LHC)
    cs2 = p.staticAntII + (1 - p.staticAntII - p.staticAntI) * LHC

    return [cs2, cs1, free]

# ====================================================================== #
# Reaction rates
# ====================================================================== #
def _ps2states(p, PQox, PQred, LHC, PSIItot, Q):
    L = LII(p, LHC)
    k2 = p.k2
    kF = p.kF
    kH = p.kH * Q + p.kH0
    k3p = p.kPQred * PQox
    k3m = p.kPQred * PQred / Keq_PQred(p)
    M = np.array(
        [
            [-L - k3m, kH + kF, k3p, 0],
            [L, -(kH + kF + k2), 0, 0],
            [0, 0, L, -(kH + kF)],
            [1, 1, 1, 1],
        ]
    )
    A = np.array([0, 0, 0, PSIItot])
    B0, B1, B2, B3 = np.linalg.solve(M, A)
    return B0, B1, B2, B3


def ps2states(p, PQox, PQred, LHC, PSIItot, Q):
    """
    QSSA, calculates the states of the photosystem II
    accepts values:
        Pox: oxidised fraction of the PQ pool (PQH2)
        Q: quencher
        L: light, int or array of the n x 1 dimension, that gives the light intensity

        returns:
        B: array of arrays with the states of PSII; rows: time, columns states: 1 and 3 excited
    """

    if isinstance(PSIItot, (int, float)):
        return np.array(_ps2states(p, PQox, PQred, LHC, PSIItot, Q))
    else:
        return np.array(
            [
                _ps2states(p, po, pr, lh, ps, q)
                for po, pr, lh, ps, q in zip(PQox, PQred, LHC, PSIItot, Q)
            ]
        ).T


def ps1states(p, PC, PCred, Fd, Fdred, LHC):
    """ 
    QSSA calculates open state of PSI
    depends on reduction states of plastocyanin and ferredoxin
    C = [PC], F = [Fd] (ox. forms)
    accepts: light, y as an array of arrays
    returns: array of PSI open
    """
    L = LI(p, LHC)

    A1 = p.PSItot / (1 + L/(p.kFdred * Fd) + (1 + Fdred/(Keq_FAFd(p) * Fd))
                      * (PC/(Keq_PCP700(p) * PCred)
                         + L/(p.kPCox * PCred))
    )
    return A1

###############################################################################
# method to calculate cross sections
###############################################################################
def LI(p, LHC):
    l1 = p.staticAntI * light(p) * absorptions(p)[1] + \
         p.prob_attach * (1 - p.staticAntII - p.staticAntI) * (1-LHC) * light(p) * absorptions(p)[2]
    return l1

def LII(p, LHC):
    l2 = p.staticAntII *  light(p) * absorptions(p)[0] + \
        (1 - p.staticAntII - p.staticAntI) * LHC  * light(p) * absorptions(p)[2]
    return l2

###############################################################################
# Reaction rates
###############################################################################
def vDegradation(p, P, Pred, LHC, B, Q):
    """ rate of damage to photosystem II, 
    proportional to the occupation of 'closed' states"""
    B = ps2states(p, P, Pred, LHC, B, Q)
    return p.kdeg*(B[1]+B[3])

def vRepair(p, B):
    """ rate of the repair of photosystem 
    krep as in Nikolaou et al. 2015"""
    return p.krep * (1-B/p.PSIItot)
            
def vPS2(p, P, Pred, LHC,  B, Q):
    """ reaction rate constant for photochemistry """
    #Q = quencher(p,Q,H)
    L = LII(p, LHC)
    B = ps2states(p, P, Pred, LHC,  B, Q)
    v = 0.5 * p.k2 * B[1]
    return v

def vPS1(p, PC, PCred, Fd, Fdred, LHC):
    """ reaction rate constant for open PSI """
    L = LI(p, LHC)
    A = ps1states(p, PC, PCred, Fd, Fdred, LHC)
    v = L * A
    return v

def oxygen(p, t):
    """ return oxygen and NDH concentration as a function of time
    used to simulate anoxia conditions as in the paper"""
    if p.ox == True:
        ''' by default we assume constant oxygen supply'''
        return p.O2ext, p.kNDH
    else:
        if t<= p.Ton or t>=p.Toff:
            return p.O2ext, 0
        else:
            return 0, p.kNDH

def vPTOX(p, t, Pred):
    """ calculates reaction rate of PTOX """
    v = Pred * p.kPTOX * oxygen(p, t)[0] 
    return v

def vNDH(p, t, Pox):
    """ 
    calculates reaction rate of PQ reduction under absence of oxygen
    can be mediated by NADH reductase NDH
    """
    v = oxygen(p, t)[1] * Pox
    return v

def _vB6f(p, Pox, Pred, PC, PCred, H):
    """ calculates reaction rate of cytb6f """
    ph = pH(H)
    Keq = Keq_cytb6f(p,ph)
    v = max(p.kCytb6f * (Pred * PC**2 - (Pox * PCred**2)/Keq), -p.kCytb6f)
    return v


def vB6f(p, Pox, Pred, PC, PCred, H):
   
    if isinstance(Pox, (int, float)):
        return np.array(_vB6f(p, Pox, Pred, PC, PCred, H))
    else:
        return np.array(
            [
                _vB6f(p, po, pr, pc, pcred, h)
                for po, pr, pc, pcred, h in zip(Pox, Pred, PC, PCred, H)
            ]
        ).T


def vCyc(p, Pox, Fdred):
    """
    calculates reaction rate of cyclic electron flow
    considered as practically irreversible
    """
    v = p.kcyc * ((Fdred**2) * Pox)
    return v

def vFNR(p, Fd, Fdred, NADPH, NADP):
    """
    Reaction rate mediated by the Ferredoxin—NADP(+) reductase (FNR)
    Kinetic: convenience kinetics Liebermeister and Klipp, 2006
    Compartment: lumenal side of the thylakoid membrane
    Units:
    Reaction rate: mmol/mol Chl/s
    [F], [Fdred] in mmol/mol Chl/s
    [NADPH] in mM
    """
    fdred = Fdred/p.KM_FNR_F
    fdox = Fd/p.KM_FNR_F
    nadph = NADPH/p.KM_FNR_N  # NADPH requires conversion to mmol/mol of chlorophyll
    nadp = NADP/p.KM_FNR_N
    v = (p.EFNR * p.kcatFNR *
        ((fdred**2) * nadp - ((fdox**2) * nadph) / Keq_FNR(p)) /
        ((1+fdred+fdred**2) * (1+nadp) + (1+fdox+fdox**2) * (1+nadph) - 1))
    return v

def vLeak(p, Hlf):
    """ 
    rate of leak of protons through the membrane
    """
    v = p.kLeak * (Hlf - pHinv(p.pHstroma))
    return v

def vSt12(p, Pox,Ant):
    """ 
    reaction rate of state transitions from PSII to PSI
    Ant depending on module used corresponds to non-phosphorylated antennae
    or antennae associated with PSII
    """
    kKin = p.kStt7 * ( 1 / (1 + ((Pox /p.PQtot)/p.KM_ST)**p.n_ST))
    v = kKin * Ant
    return v

def vSt21(p, Ant):
    """
    reaction rate of state transitions from PSI to PSII
    """
    v = p.kPph1 * (1 - Ant)
    return v

def vATPact(p, t, Einact):
    """ replaces rate constant kATPsynth in the raction of ATP synthase
        increases ATP activity in light and decreases in darkness
    """
    switch = 1.0 * (light(p) >= 1)
    vf = p.kActATPase * switch * Einact
    return vf

def vATPdeact(p, t, Eact):
    """ replaces rate constant kATPsynth in the raction of ATP synthase
        increases ATP activity in light and decreases in darkness
    """
    switch = 1.0 * (light(p) >= 1)
    vr = p.kDeactATPase * (1 - switch) * Eact
    return vr


def vATPsynthase(p, ATP, ADP, H, E):
    """
    Reaction rate of ATP production
    Kinetic: simple mass action with PH dependant equilibrium
    Compartment: lumenal side of the thylakoid membrane
    Units:
    Reaction rate: mmol/mol Chl/s
    """
    ph = pH(H)
    v = p.kATPsynth * (ADP - ATP / Keq_ATP(p, ph)) * E
    return v

def vATPconsumption(p, ATP):
    """ ATP consuming reaction modelled by mass-action kinetics"""
    v = p.kATPcons * ATP
    return v

def vNADPHconsumption(p, N):
    v = p.kNADPHcons * N
    return v

def vQuencherBase(p, Q, H):
    vNPQdyn = H**p.nH / (H ** p.nH + pHinv(p.NPQsw)**p.nH)
    v = (1-Q) * p.kNh * vNPQdyn - p.kNr * Q
    return v    


def vDeepox(p, V, Hlf):
    """
    activity of xantophyll cycle: de-epoxidation of violaxanthin, modelled by Hill kinetics
    """
    nH = p.kHillX
    vf = p.kDeepoxV * ((Hlf ** nH)/ (Hlf ** nH + pHinv(p.kphSat) ** nH)) * V
    return vf

def vEpox(p, V, Hlf):
    """
    activity of xantophyll cycle: epoxidation
    """
    vr = p.kEpoxZ * (p.Xtot - V)
    return vr

def vLhcprotonation(p, L, Hlf):
    """
    activity of PsbS protein protonation: protonation modelled by Hill kinetics
    """
    nH = p.kHillL # shall be changed, for now =5; also half saturation of pH could be change
    vf = p.kProtonationL * ((Hlf ** nH)/ (Hlf ** nH + pHinv(p.kphSatLHC) ** nH)) * L
    return vf

def vLhcdeprotonation(p, L, Hlf):
    """
    activity of PsbS protein protonation: deprotonation
    """
    vr = p.kDeprotonation * (p.PsbStot - L)
    return vr

def vQuencher4states(p, L, V):
    """ 
    co-operatiove 4-state quenching mechanism
    
    Comment:
        seems like this can't reach 1, so I've checked the max value and normalized it
        for i in np.linspace(0,1,100):
            for j in np.linspace(0,1,100):
                a = np.hstack((a, vQuencher4states(p, i, j)))
        max(a)
    """
    Lp = p.PsbStot - L
    Z = p.Xtot - V
    ZAnt = Z / (Z + p.kZSat)
    Q = (p.gamma0 * (1-ZAnt) * L + p.gamma1 * (1-ZAnt) * Lp + p.gamma2 * ZAnt * Lp + p.gamma3 * ZAnt * L)/0.5625

    return [Q]

###############################################################################
# For now external functions

def light(p):
    '''
    :return: light intensity at certain point of time. 
    Typical PAM light function
    '''
    return p.PFD

def fluorescence(p, P, Pred, LHC, B, Q, ps2cs):
    Bst = ps2states(p, P, Pred, LHC, B, Q)
    fluo = (ps2cs * p.kF * Bst[0]) / (p.kF + p.kH0 + p.k2 + p.kH*Q) + (ps2cs * p.kF * Bst[2]) / (p.kF + p.kH0+ p.kH*Q)
    return [fluo]


def absorptions(p):
    '''
    by default uses no wavelengths, only by updating the parameter to a numeric value
    expressed in [nm], calculates the specific absorption rate
    this rate is used to multiply the cross section and light intensity to provide the amount of absorbed light
    '''
    if p.wv == False:
        return [1,1,1]
    else:
        [abscPSII, abscPSI, abscLHCII] = c.get_specific_abs(p.wv)
        absPSII = abscPSII[0]
        absPSI = abscPSI[0]
        absLHC = abscLHCII[0]
        return [absPSII, absPSI, absLHC]

###############################################################################
# Construct the model
    
m = Model(p)

m.add_compounds(compounds)

# Algebraic modules
m.add_algebraic_module("pq_alm", pqmoiety, ["PQ"],  ["PQred"])
m.add_algebraic_module("pc_alm", pcmoiety, ["PC"],  ["PCred"])
m.add_algebraic_module("fd_alm", fdmoiety, ["Fd"],  ["Fdred"])
m.add_algebraic_module("adp_alm", adpmoiety, ["ATP"],  ["ADP"])
m.add_algebraic_module("nadp_alm", nadpmoiety, ["NADPH"], ["NADP"])
m.add_algebraic_module("atpenz_alm", atpenzmoiety, ["E"], ["Einact"])
m.add_algebraic_module("vQuencher4states", vQuencher4states, ["Psbs", "Vx"], ["Q"])
m.add_algebraic_module("crosssections", crosssections, ["LHC"], ['ps2cs', 'ps1cs','freecs'])
m.add_algebraic_module("fluorescence", fluorescence, ["PQ", "PQred", "LHC",  "B", "Q", "ps2cs"], ['Fluo'])

# add light driven electron transport chain reaction rates        
m.add_reaction("vDegradation", vDegradation, {"B": -1}, ["PQ", "PQred", "LHC", "B","Q"])
m.add_reaction("vRepair", vRepair, {"B": 1}, ["B"])

m.add_reaction('vPS2', vPS2, {"PQ":-1, "H": 2/m.get_parameter("bH")}, ["PQ", "PQred", "LHC", "B","Q"])
m.add_reaction("vPS1", vPS1, {"Fd": -1, "PC": 1}, ["PC", "PCred", "Fd", "Fdred", "LHC"])
m.add_reaction("vPTOX", vPTOX, {"PQ": 1}, ["time", "PQred"])
m.add_reaction("vB6f", vB6f, {"PQ": 1, "PC": -2, "H": 4/m.get_parameter("bH")}, ["PQ", "PQred", "PC", "PCred", "H"])
m.add_reaction("vNDH", vNDH, {"PQ": -1}, ["time", "PQ"])
m.add_reaction("vCyc", vCyc, {"PQ": -1, "Fd": 2}, ["PQ", "Fdred"])
m.add_reaction("vFNR", vFNR, {"Fd": 2, "NADPH": 1}, ["Fd", "Fdred", "NADPH", "NADP"])
m.add_reaction("vLeak", vLeak, {"H": -1/m.get_parameter("bH")}, ["H"])
m.add_reaction("vSt12", vSt12, {"LHC": -1}, ["PQ", "LHC"])
m.add_reaction("vSt21", vSt21, {"LHC": 1}, ["LHC"])
m.add_reaction("vATPact", vATPact, {"E": 1}, ["time", "Einact"])
m.add_reaction("vATPdeact", vATPdeact, {"E": -1}, ["time", "E"])
m.add_reaction("vATPsynthase", vATPsynthase, {"ATP": 1, "H": -m.get_parameter("HPR")/m.get_parameter("bH")}, ["ATP", "ADP", "H", "E"])
m.add_reaction("vATPconsumption", vATPconsumption, {"ATP": -1}, ["ATP"])
m.add_reaction("vNADPHconsumption", vNADPHconsumption, {"NADPH": -1}, ["NADPH"])
m.add_reaction("vLHCprotonation", vLhcprotonation, {"Psbs": -1}, ["Psbs", "H"])
m.add_reaction("vLHCdeprotonation", vLhcdeprotonation, {"Psbs": 1}, ["Psbs", "H"])
m.add_reaction("vDeepox", vDeepox, {"Vx": -1}, ["Vx", "H"])
m.add_reaction("vEpox", vEpox, {"Vx": 1}, ["Vx", "H"])
    

        

s = Simulator(m)

# Set initial conditions using dictionary
y0 = {"B":  m.get_parameter("PSIItot"),  #photosystem II protein concentration
    "PQ": 8.5,  # oxidised plastoquinone
    "PC": 2.,  # oxidised plastocyan
    "Fd": 2.5,  # oxidised ferrodoxin
    "E": 0.1,
    "ATP": 30.,  # stromal concentration of ATP
    "NADPH": 7.,  # stromal concentration of NADPH
    "H": 0.0002,  # lumenal protons
    "LHC": 0.9,  # non-phosphorylated antenna
    "Psbs": 0.9, # PsBs
    "Vx": 0.9}

s.set_initial_conditions(y0)
    

